import Screen from "../models/screenModel.js";
import Campaign from "../models/campaignModel.js";
import Media from "../models/mediaModel.js";
import Brand from "../models/brandModel.js";
import User from "../models/userModel.js";
import ScreenLogs from "../models/screenLogsModel.js";
import {
  CAMPAIGN_STATUS_ACTIVE,
  CAMPAIGN_STATUS_PAUSE,
} from "../Constant/campaignStatusConstant.js";
import {
  changeCampaignStatusAsActive,
  changeCampaignStatusAsDeleted,
  changeCampaignStatusAsPause,
  createDefaultCampaign,
  createNewCampaignHepler,
} from "../helpers/campaignHelper.js";
import { addNewTransaction } from "../helpers/userWalletHelper.js";

export async function addNewCampaign(req, res) {
  try {
    console.log("addNewCampaign called! body : ");
    const user = req.body.user; // ally
    const screenIds = req.body.screenIds;
    const mediaId = req.body.mediaId;

    const media = await Media.findById(mediaId);
    if (!media) res.status(404).send({ message: "Media Not Found" });
    const cid = media.media.split("/")[4];

    for (let screenId of screenIds) {
      const screen = await Screen.findById(screenId);
      if (!screen) res.status(404).send({ message: "Screen Not Found" });

      const findCampaign = await Campaign.findOne({
        cid,
        screen: screenId,
        status: { $in: [CAMPAIGN_STATUS_ACTIVE, CAMPAIGN_STATUS_PAUSE] },
      });

      if (!findCampaign) {
        const data = {
          campaignName: req.body.campaignName || "Your Campaign",
          brandName: req.body.brandName || "Your Brand",
          startDate: req.body.startDate || new Date(),
          endDate: req.body.endDate || new Date(),
          isDefaultCampaign: req.body.isDefaultCampaign,
          status: CAMPAIGN_STATUS_ACTIVE,
          noOfDays: req.body.noOfDays,
          cid: cid,
        };
        let campaign;
        if (req.body.isDefaultCampaign) {
          campaign = createDefaultCampaign({ data, screen, media, user });
        } else {
          campaign = await createNewCampaignHepler({
            data,
            screen,
            media,
            user,
          });
        }
        screen.campaigns.push(campaign._id);
        await screen.save();
        console.log("Create created by screen owner :  screen");
      } else {
        console.log("Campaign all ready present, so we cann't create again");
      }
    }
    return res.status(201).send({
      message: "Campaign Created successfull",
      campaign: {},
    });
  } catch (error) {
    console.log("error : ", error);
    return res
      .status(500)
      .send({ message: `Campaign router error ${error.message}` });
  }
}

// campaign list by screen id

// All campaign list by screen id with status active, pause, deleted, completed
export async function getAllCampaignListByScreenId(req, res) {
  try {
    console.log("getAllCampaignListByScreenId called!");

    const screenId = req.params.id;
    const campaignList = await Campaign.find({
      screen: screenId,
    }).sort({ status: 1 });
    if (!campaignList) {
      return res.status(401).send({ message: "campaign not found" });
    }
    return res.status(200).send(campaignList);
  } catch (error) {
    return res
      .status(401)
      .send({ message: `campaign router error, ${error.message}` });
  }
}

// All campaign list by screen id with status ACTIVE OR PAUSE only
export async function getAllActiveOrPauseCampaignListByScreenId(req, res) {
  try {
    console.log("getAllActiveOrPauseCampaignListByScreenId called!");

    const screenId = req.params.id;
    const campaignList = await Campaign.find({
      screen: screenId,
      status: { $in: [CAMPAIGN_STATUS_ACTIVE, CAMPAIGN_STATUS_PAUSE] },
    }).sort({ status: 1 });
    if (!campaignList) {
      return res.status(401).send({ message: "campaign not found" });
    }
    return res.status(200).send(campaignList);
  } catch (error) {
    return res
      .status(401)
      .send({ message: `campaign router error, ${error.message}` });
  }
}

// All campaign list by screen id with status ACTIVE only
export async function getAllActiveCampaignListByScreenId(req, res) {
  try {
    console.log("getAllActiveCampaignListByScreenId called!");

    const screenId = req.params.id;
    const campaignList = await Campaign.find({
      screen: screenId,
      status: CAMPAIGN_STATUS_ACTIVE,
    }).sort({ status: 1 });
    if (!campaignList) {
      return res.status(401).send({ message: "campaign not found" });
    }
    return res.status(200).send(campaignList);
  } catch (error) {
    return res
      .status(401)
      .send({ message: `campaign router error, ${error.message}` });
  }
}

// Get campaign list on the basic of User id;

// All campaign list by user id with status active, pause, deleted, completed
export async function getAllCampaignListByUserId(req, res) {
  //tested in ui
  try {
    console.log("getAllCampaignListByUserId called!");
    const userId = req.params.id;
    const campaignList = await Campaign.find({
      ally: userId,
    }).sort({ status: 1 });
    if (!campaignList) {
      return res.status(401).send({ message: "campaign not found" });
    }
    console.log("campaign found : ", campaignList.length);
    return res.status(200).send(campaignList);
  } catch (error) {
    return res
      .status(401)
      .send({ message: `campaign router error, ${error.message}` });
  }
}

// All campaign list by user id with status ACTIVE OR PAUSE only
export async function getAllActiveOrPauseCampaignListByUserId(req, res) {
  try {
    console.log("getAllActiveOrPauseCampaignListByUserId called!");
    const userId = req.params.id;
    const campaignList = await Campaign.find({
      ally: userId,
      status: { $in: [CAMPAIGN_STATUS_ACTIVE, CAMPAIGN_STATUS_PAUSE] },
    });
    if (!campaignList) {
      return res.status(401).send({ message: "campaign not found" });
    }
    return res.status(200).send(campaignList);
  } catch (error) {
    console.log("error : ", error);
    return res
      .status(401)
      .send({ message: `campaign router error, ${error.message}` });
  }
}

// All campaign list by user id with status ACTIVE only //
export async function getAllActiveCampaignListByUserId(req, res) {
  try {
    console.log("getAllActiveCampaignListByUserId called!");

    const userId = req.params.id;
    const campaignList = await Campaign.find({
      ally: userId,
      status: CAMPAIGN_STATUS_ACTIVE,
    }).sort({ status: 1 });
    if (!campaignList) {
      return res.status(401).send({ message: "campaign not found" });
    }
    return res.status(200).send(campaignList);
  } catch (error) {
    return res
      .status(401)
      .send({ message: `campaign router error, ${error.message}` });
  }
}

// get campaign list which running on this screen whose name was given
export async function getCampaignListByScreenName(req, res) {
  try {
    const screenName = req.params.name;
    const screen = await Screen.findOne({ name: screenName });
    if (!screen) return res.status(401).send({ message: "campaign not found" });

    const campaignList = await Campaign.find({ screen: screen._id });
    if (!campaignList)
      return res.status(401).send({ message: "campaign not found" });

    return res.status(200).send(campaignList);
  } catch (error) {
    return res.status(500).send({ message: `campaign router error, ${error}` });
  }
}

// get all active campaign list
export async function getAllActiveCampaignList(req, res) {
  try {
    const allCampaign = await Campaign.find({ status: { $eq: "Active" } });
    if (allCampaign) {
      return res.status(200).send(allCampaign);
    } else {
      return res.status(404).send({ message: "No campaign found" });
    }
  } catch (error) {
    return res
      .status(500)
      .send({ message: `campaign router error ${error.message}` });
  }
}

// change campaign status (Deleted, Pause , Resume)from screen
export async function changeCampaignStatus(req, res) {
  try {
    const campaignId = req.params.id;
    const campaignStatus = req.params.status;

    const campaign = await Campaign.findById(campaignId);
    console.log(
      "changeCampaignStatus function called! ",
      campaignStatus,
      campaign.campaignName
    );
    if (!campaign || !campaignStatus) {
      return res
        .status(400)
        .send("Unauthorize access for deleting this campaign");
    }

    if (campaignStatus === "Deleted") {
      const updatedCampaign = await changeCampaignStatusAsDeleted(campaign);
      return res.status(200).send(updatedCampaign);
    } else if (campaignStatus === "Pause") {
      const updatedCampaign = await changeCampaignStatusAsPause(campaign);
      return res.status(200).send({ message: "Campaign Paused " });
    } else if (campaignStatus === "Resume") {
      const updatedCampaign = await changeCampaignStatusAsActive(campaign);
      res.status(200).send({ message: "Campaign Resume " });
    }
  } catch (error) {
    console.log("Errro in update campaign status : ", error);
    return res
      .status(500)
      .send({ message: `Campaign router error ${error.message}` });
  }
}

export const getAllCampaignListUserId = async (req, res) => {
  try {
    const campaigns = await Campaign.find({ ally: req.params.allyId });
    return res.status(200).send(campaigns);
  } catch (error) {
    console.log("Error in getAllCampaignListUserId : ", error);
    return res
      .status(500)
      .send({ message: `Erro in getAllCampaignListUserId ${error.message}` });
  }
};

//get campaign detail by campaignId
export async function getCampaignDetailByCampaignId(req, res) {
  try {
    const campaignId = req.params.id;
    const campaign = await Campaign.findById(campaignId);
    // console.log("getCampaignDetailByCampaignId called! : ", campaign);

    if (campaign) {
      return res.status(200).send(campaign);
    } else {
      return res.status(404).send({ message: "No campaign found" });
    }
  } catch (error) {
    return res
      .status(500)
      .send({ message: `Campaign router error ${error.message}` });
  }
}

export async function getCampaignAndMediaListByScreenId(req, res) {
  try {
    const screenId = req.params.id;
    const screen = await Screen.findById(screenId);
    if (!screen) return res.status(404).send({ message: "No Screen found" });
    let data = [];
    //let data = [{media : {}, campaign : {}},{media : {}, campaign : {}}];
    if (screen.campaigns.length > 0) {
      for (let index = 0; index < screen.campaigns.length; index++) {
        const campaignId = screen.campaigns[index];
        let campaign, media;
        campaign = await Campaign.findById(campaignId);
        if (campaign) {
          media = await Media.findById(campaign.media);
        }
        if (campaign && media) {
          data.push({
            media,
            campaign,
          });
        }
      }
      return res.status(200).send(data);
    } else {
      return res.status(404).send({ message: "No Screen found", data });
    }
  } catch (error) {
    return res
      .status(500)
      .send({ message: `Campaign router error ${error.message}` });
  }
}

export async function updateCampaignById(req, res) {
  try {
    const campaign = await Campaign.findById(req.params.id);
    if (!campaign) res.status(404).send({ message: "No Screen found", data });
    campaign.thumbnail =
      "https://ipfs.io/ipfs/Qmf1mxa1NMYC2LCUoQabntCJubXjDrXtVn4Jsin8F3cdos" ||
      req.body.thumbnail;
    const updatededCampaign = await campaign.save();
    return res.status(200).send(updatededCampaign);
  } catch (error) {
    return res
      .status(500)
      .send({ message: `Campaign router error ${error.message}` });
  }
}

export async function getFilteredCampaignListBydateRange(req, res) {
  try {
    const campaigns = await Campaign.find({
      startDate: {
        $gte: req.params.startValue,
        $lt: req.params.endValue,
      },
      master: req.params.userId,
      screen: req.params.screenId,
    });

    return res.status(200).send(campaigns);
  } catch (error) {
    return res
      .status(500)
      .send({ message: `Campaign router error ${error.message}` });
  }
}

// get campaign details alogn with all screens with this campaign
export async function getCampaignDetailWithCidAndCampaignName(req, res) {
  try {
    const cid = req.params.cid;
    const campaignName = req.params.campaignName;
    const campaigns = await Campaign.find({ cid, campaignName });

    if (campaigns.length === 0) {
      return res.status(404).send({ message: "Campaigns not found" });
    }

    const results = [];

    for (let eachCampaign of campaigns) {
      const screen = await Screen.findById(eachCampaign.screen);
      results.push({
        campaign: eachCampaign,
        screen: screen,
      });
    }
    return res.status(200).send(results);
  } catch (error) {
    return res.status(500).send({
      message: `Campaign controller error at getCampaignDetailWithScreenList ${error}`,
    });
  }
}

export async function getAllCampaignListByBrandId(req, res) {
  try {
    const brandId = req.params.brandId;
    const brand = await Brand.findOne({ _id: brandId });
    const campaignList = await Campaign.find({
      ally: brand.user,
    });
    if (!campaignList) {
      return res.status(401).send({ message: "campaign not found" });
    }
    return res.status(200).send(campaignList);
  } catch (error) {
    return res
      .status(404)
      .send({ message: `campaign router error, ${error.message}` });
  }
}
// top camapigns
export async function getTopCampaigns(req, res) {
  try {
    console.warn("getTopCampaigns called");
    const topmedias = await Campaign.find({})
      .sort({ "master.rating": -1 })
      .limit(3);
    res.status(200).send(topmedias);
  } catch (error) {
    console.error(error);
    return res.status(500).send(`Error in getTopCampaigns: ${error}`);
  }
}
export async function getCampaignLogsByScreenIdAndCid(req, res) {
  try {
    const start = req.query.start || 0;
    const end = req.query.end || 50;
    const screenId = req.query.screenId;
    const cid = `${req.query.cid}.mp4`;

    const screenLog = await ScreenLogs.findOne({ screen: screenId });
    if (!screenLog) return res.status(200).send({ last50: [], totalCount: 0 });
    const result = screenLog.playingDetails?.filter(
      (data) => data.playVideo === cid
    );
    const last50 = result.reverse().slice(start, end);

    const totalCount = result.length;
    return res.status(200).send({ last50, totalCount });
  } catch (error) {
    console.log("error : ", error);
    return res.status(500).send({
      message: `Error in getCampaignLogsByScreenIdAndCid ${error.message}`,
    });
  }
}

export async function getCamDataByCampaignId(req, res) {
  try {
    const start = req.query.start || 0;
    const end = req.query.end || 50;
    const campaignId = req.query.campaignId;

    const campaign = await Campaign.findOne({ _id: campaignId });
    const screenLog = await ScreenLogs.findOne({ screen: campaign.screen });
    if (!screenLog) return res.status(200).send({ screenCamData: [] });

    const result = {
      camData: screenLog.camData,
      playbackLog: screenLog.playingDetails?.filter(
        (data) => data.playVideo === campaign.cid
      ),
    };
    const last50 = result.camData.reverse().slice(start, end);

    // const totalCount = result.length;
    return res.status(200).send({ screenCamData: last50, allCamData: result });
  } catch (error) {
    console.log("error : ", error);
    return res.status(500).send({
      message: `Error in getCampaignCamDataByScreenIdAndCid ${error.message}`,
    });
  }
}

export async function getCamDataByCid(req, res) {
  try {
    const start = req.query.start || 0;
    const end = req.query.end || 50;
    const cid = req.query.cid;
    console.log(cid);
    const campaigns = await Campaign.find({ cid: cid });
    const screenIds = [];
    campaigns.map((camp) => {
      screenIds.push(camp.screen);
    });
    // console.log(screenIds);
    const screenLogs = [];
    const screenCamLogs = [];
    for (let i = 0; i < screenIds.length; i++) {
      const screenLog = await ScreenLogs.findOne({ screen: screenIds[i] });

      screenLog.playingDetails
        .filter((data) => data.playVideo === cid)
        .map((d) => {
          screenLogs.push(d);
        });
      screenLog.camData.map((d) => {
        screenCamLogs.push(d);
      });
      console.log(screenLog.playingDetails.length);
    }

    if (screenLogs.length === 0)
      return res.status(200).send({ screenCamData: [] });
    const result = {
      camData: screenCamLogs,
      playbackLog: screenLogs.filter((data) => data.playVideo === cid),
    };
    // console.log(result);

    const last50 = result.camData.reverse().slice(start, end);

    // const totalCount = result.length;
    return res.status(200).send({ screenCamData: last50, allCamData: result });
  } catch (error) {
    console.log("error : ", error);
    return res.status(500).send({
      message: `Error in getCampaignCamDataByScreenIdAndCid ${error.message}`,
    });
  }
}
