import UserQuery from "../models/UserQueryModel.js";

export const addNewQuery = async (req, res) => {
  try {
    const query = new UserQuery({
      user: req.user._id,
      name: req.body.name,
      email: req.body.email,
      subject: req.body.subject,
      message: req.body.message,
    });
    const newQuery = await query.save();
    console.log("New Query :", newQuery);
    return res.status(200).send(newQuery);
  } catch (error) {
    console.log("Error : ", error);
    return res
      .status(500)
      .send({ message: `addNewQuery controller ${error.message}` });
  }
};

export const getAllQueryByUser = async (req, res) => {
  try {
    console.log("getAllQueryByUser called!");
    const userId = req.user._id;
    const queries = await UserQuery.find({ user: userId });
    console.log("queries : ", queries);
    return res.status(200).send(queries);
  } catch (error) {
    console.log("Error : ", error);
    return res
      .status(500)
      .send({ message: `getAllQueryByUser controller ${error.message}` });
  }
};

export const getAllQuery = async (req, res) => {
  try {
    const queries = await UserQuery.find();
    return res.status(200).send(querys);
  } catch (error) {
    console.log("Error : ", error);
    return res
      .status(500)
      .send({ message: `getAllQueryByUser controller ${error.message}` });
  }
};
