import {
  CAMPAIGN_STATUS_ACTIVE,
  CAMPAIGN_STATUS_PENDING,
  TRANSACTION_TYPE_DEBIT,
} from "../Constant/campaignStatusConstant.js";
import Campaign from "../models/campaignModel.js";
import Screen from "../models/screenModel.js";
import { getFilterScreensHelper } from "../helpers/screenHelper.js";
import { createNewCampaignHepler } from "../helpers/campaignHelper.js";
import { createCampaignPleaHelper } from "../helpers/pleaHelper.js";
import {
  addNewTransaction,
  generateRemarkForDabitOnCreateCampaign,
} from "../helpers/userWalletHelper.js";

export async function addNewCampaignForMultipleScreen(req, res) {
  try {
    console.log("addNewCampaignForMultipleScreen  called : ");

    const cid = req.body.cid;
    const user = req.body.user;
    const media = req.body.media;
    const campaignName = req.body.campaignName;
    const campaignGoal = req.body.campaignGoal;
    const startDateAndTime = req.body.startDateAndTime;
    const endDateAndTime = req.body.endDateAndTime;
    const brandName = req.body.brandName;
    const noOfDays = Math.round(req.body.noOfDays); // 2.5 >= 3
    const screens = req.body.screens;
    const isDefaultCampaign = req.body.isDefaultCampaign || false;
    const createdCampaign = [];
    const campaignAllreadyPresent = [];

    for (let screenId of screens) {
      const screen = await Screen.findById(screenId);
      const findCampaign = await Campaign.findOne({
        cid,
        screen: screen?._id,
      });
      // if campaign present with same cid and screenis
      if (findCampaign) {
        campaignAllreadyPresent.push(findCampaign);
      } else {
        const data1 = {
          campaignName: campaignName || "campaign Name",
          startDate: startDateAndTime || new Date(),
          endDate: endDateAndTime || new Date(),
          isDefaultCampaign: isDefaultCampaign,
          noOfDays,
          brandName,
          cid,
          campaignGoal,
          additionalInfo: req.body.additionalInfo,
          status:
            screen.master != user._id
              ? CAMPAIGN_STATUS_PENDING
              : CAMPAIGN_STATUS_ACTIVE,
        };

        const campaign = await createNewCampaignHepler({
          data: data1,
          user,
          screen,
          media,
        });
        console.log("new campaign created successfully! ", campaign);
        createdCampaign.push(campaign);

        // Now send plea request to screen owner of this screen, if user itself is not screen owner
        if (screen.master != user._id) {
          //send campaign plea to that screen owner
          await createCampaignPleaHelper({ user, screen, cid, campaign });
          // calling this function to deduct amount from wallet of ally user
          const transaction = await addNewTransaction({
            toUser: campaign.ally, //
            amount: campaign.vault,
            campaign,
            remark: generateRemarkForDabitOnCreateCampaign({
              amount: campaign.vault,
              campaignName: campaign.campaignName,
              rentPerSlot: campaign.rentPerSlot,
              screenName: screen.name,
              totalSlotBooked: campaign.totalSlotBooked,
            }),
            type: TRANSACTION_TYPE_DEBIT,
            paymentStatus: true,
          });
          console.log("Transaction success for addNewCampaign");
        }
      }
    }
    return res.status(200).send({
      message: "Campaigns created successfuly!",
      createdCampaign,
      campaignAllreadyPresent,
    });
  } catch (error) {
    console.error(error);
    return res.status(500).send({
      message: `addNewCampaignForMultipleScreen router error ${error.message}`,
    });
  }
}

export async function filterScreensBasedOnAudiancesProfile(req, res) {
  try {
    console.log("filterScreensBasedOnAudiancesProfile called!");
    const location = req.body.cities.split(",").map((city) => {
      const str = city.trim().toLowerCase();
      return str.charAt(0).toUpperCase() + str.slice(1);
    }); // note first remove spaces amd convert first cahr is capi

    const query = {
      employmentStatus: req.body.employmentTypes,
      mobility: req.body.croudMobability,
      screenHighlights: req.body.screenHighlights,
      location: location,
      averageAgeGroup: req.body.ageRange,
      averageDailyFootfall: req.body.numberOfAudiances,
      category: req.body.category || "",
    };

    const screens = await getFilterScreensHelper(query);
    return res.status(200).send(screens);
  } catch (error) {
    console.error(error);
    return res.status(500).send({
      message: `filterScreensBasedOnAudiancesProfile router error ${error.message}`,
    });
  }
}
