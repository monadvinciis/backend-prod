import mongoose from "mongoose";
import { CAMPAIGN_STATUS_PENDING } from "../Constant/campaignStatusConstant.js";

const playingDataSchema = new mongoose.Schema(
  {
    deviceInfo: {},
    playTime: { type: Date },
  },
  {
    timestamps: true,
  }
);

const campaignSchema = new mongoose.Schema(
  {
    media: { type: mongoose.Schema.Types.ObjectId, ref: "Media" },
    video: { type: String, required: true },
    cid: { type: String, required: true },
    thumbnail: { type: String, required: true },
    screen: { type: mongoose.Schema.Types.ObjectId, ref: "Screen" },
    campaignName: { type: String, required: true, default: "campaign name" },
    screenName: { type: String },
    brandName: { type: String },
    uploadedBy: { type: String },

    startDate: { type: Date },
    endDate: { type: Date },
    master: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User",
      default: null,
    },
    ally: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User",
      default: null,
    }, // campaign creator
    brandWalletAddress: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "UserWallet",
    }, // campaign creator wallet id
    masterWalletAddress: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "UserWallet",
    }, // screen owner wallet id

    revenue: { type: Number, default: 0 },
    vault: { type: Number, default: 0 },
    totalSlotBooked: { type: Number, default: 0 },
    remainingSlots: { type: Number, default: 0 },
    totalSlotPlayed: { type: Number, default: 0 },
    slotPlayedPerDay: { type: Number, default: 0 },

    rentPerSlot: { type: Number, default: 0 },
    // date will came from screen [rentPerDay ,slotsPlayPerDay]
    rentPerDay: { type: Number, default: 0 },
    slotsPlayPerDay: { type: Number, default: 0 },

    totalAmount: { type: Number, default: 0 },

    isSlotBooked: { type: Boolean, default: false },
    isDefaultCampaign: { type: Boolean, default: false }, // screen.master === campaign.ally
    paidForSlot: { type: Boolean, default: false },
    status: { type: String, default: CAMPAIGN_STATUS_PENDING },

    additionalInfo: {
      campaignGoal: { type: String },
      cities: { type: String },
      highlights: { type: String },
      budget: { type: Number, default: 0 },
      category: { type: String },
      crowdMobilityType: [{ type: String }],
      ageRange: [{ type: Number }],
    },

    campaignLogs: [playingDataSchema],
  },
  {
    timestamps: true,
  }
);

const Campaign = mongoose.model("Campaign", campaignSchema);

export default Campaign;
