import mongoose from "mongoose";
import { PLEA_STATUS_PENDING } from "../Constant/pleaRequestTypeConstant.js";

const pleaSchema = new mongoose.Schema(
  {
    video: { type: String },
    campaign: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Campaign",
    },
    from: { type: mongoose.Schema.Types.ObjectId, ref: "User", required: true }, //from
    to: { type: mongoose.Schema.Types.ObjectId, ref: "User", default: null }, // to
    pleaType: {
      type: String,
      required: true,
      default: "SCREEN_ALLY_PLEA",
      enum: ["SCREEN_ALLY_PLEA", "CAMPAIGN_ALLY_PLEA", "COUPON_REDEEM_PLEA"],
    },
    status: { type: String, default: PLEA_STATUS_PENDING },
    screen: { type: mongoose.Schema.Types.ObjectId, ref: "Screen" } || null,
    content: { type: String },
    remarks: [],
  },
  {
    timestamps: true,
  }
);

const Plea = mongoose.model("Plea", pleaSchema);

export default Plea;
