import mongoose from "mongoose";

const userQuery = new mongoose.Schema({
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User",
    default: null,
  },
  name: { type: String, required: true },
  email: { type: String, required: true },
  subject: { type: String, required: true },
  message: { type: String, required: true },
  status: { type: String, required: true, default: "Active" },
});

const UserQuery = mongoose.model("UserQuery", userQuery);

export default UserQuery;
