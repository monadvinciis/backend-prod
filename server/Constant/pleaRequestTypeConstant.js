// before adding campaign on  screen need to ask first or take permition from that screen owner
export const CAMPAIGN_ALLY_PLEA = "CAMPAIGN_ALLY_PLEA";
export const COUPON_REDEEM_PLEA = "COUPON_REDEEM_PLEA";
// before deploying to screen, get first SCREEN_ALLY_PLEA from scren owner, so that next time to brand, no need to ask again again to from screen ownet to accept my campaign
export const SCREEN_ALLY_PLEA = "SCREEN_ALLY_PLEA";

export const PLEA_STATUS_APPROVED = "Approved";
export const PLEA_STATUS_REJECTED = "Rejected";
export const PLEA_STATUS_PENDING = "Pending";
