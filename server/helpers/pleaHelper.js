import { CAMPAIGN_ALLY_PLEA } from "../Constant/pleaRequestTypeConstant.js";
import Plea from "../models/pleaModel.js";
import User from "../models/userModel.js";
import mongoose from "mongoose";

export const createCampaignPleaHelper = async ({
  user,
  screen,
  cid,
  campaign,
}) => {
  try {
    const fromUser = await User.findById(user._id);

    const plea = new Plea({
      _id: new mongoose.Types.ObjectId(),
      from: user._id,
      to: screen.master,
      screen: screen._id,
      pleaType: CAMPAIGN_ALLY_PLEA,
      content: `I would like to request an Campaign plea for this ${screen.name} screen`,
      status: false,
      reject: false,
      blackList: false,
      remarks: `${user.name} has requested an Campaign plea for ${screen.name} screen`,
      video: `https://ipfs.io/ipfs/${cid}`,
      campaign: campaign._id,
    });

    const savedPlea = await plea.save();
    screen.pleas.push(savedPlea);
    fromUser.pleasMade.push(savedPlea);
    await screen.save();
    await fromUser.save();
    console.log("Campaign plea create success");

    return Promise.resolve();
  } catch (error) {
    return Promise.reject(error);
  }
};

export const deleteAllPleaByUserId = () => {
  try {
  } catch (error) {}
};

export const deleteAllPleaByScreenId = () => {
  try {
  } catch (error) {}
};
