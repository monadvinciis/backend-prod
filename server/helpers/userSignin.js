import { generateToken } from "../utils/authUtils.js";

export const returnUserSignIn = (updatedUser) => {
  return {
    _id: updatedUser._id,
    name: updatedUser.name,
    businessName: updatedUser.businessName,
    gst: updatedUser.gst,
    email: updatedUser.email,
    avatar: updatedUser.avatar,
    phone: updatedUser.phone,
    address: updatedUser.address,
    districtCity: updatedUser.districtCity,
    municipality: updatedUser.municipality,
    pincode: updatedUser.pincode,
    stateUt: updatedUser.stateUt,
    country: updatedUser.country,
    isItanimulli: updatedUser.isItanimulli,
    isMaster: updatedUser.isMaster,
    isCreator: updatedUser.isCreator,
    creator: updatedUser.creator,
    isBrand: updatedUser.isBrand,
    brand: updatedUser.brand,

    defaultWallet: updatedUser.defaultWallet,
    wallets: updatedUser.wallets,
    pwaInstalled: updatedUser.pwaInstalled,

    screens: updatedUser.screens,
    screensSubscribed: updatedUser.screensSubscribed,
    screensLiked: updatedUser.screensLiked,
    screensFlagged: updatedUser.screensFlagged,
    medias: updatedUser.medias,
    mediasLiked: updatedUser.mediasLiked,
    mediasFlagged: updatedUser.mediasFlagged,
    mediasViewed: updatedUser.mediasViewed,
    userWallet: updatedUser.userWallet,

    pleasMade: updatedUser.pleasMade,
    alliedScreens: updatedUser.alliedScreens,
    additionalInfo: updatedUser.additionalInfo,
    token: generateToken(updatedUser),

    createdAt: updatedUser.createdAt,
  };
};
