import Screen from "../models/screenModel.js";

export const getFilterScreensHelper = async ({
  screenHighlights = [],
  averageAgeGroup = [],
  mobility = [],
  employmentStatus = [],
  genders = [],
  averagePurchasePower = [],
  averageDailyFootfall = [],
  location = [],
  category = "",
}) => {
  try {
    const averageDailyFootfallFilter =
      averageDailyFootfall.length > 1
        ? {
            "additionalData.averageDailyFootfall": {
              $lte: averageDailyFootfall[1],
              $gte: averageDailyFootfall[0],
            },
          }
        : {};
    const averagePurchasePowerGreaterThen =
      averagePurchasePower.length > 1
        ? {
            "additionalData.footfallClassification.averagePurchasePower.start":
              {
                $gte: Number(averagePurchasePower[0]),
              },
          }
        : {};
    const averagePurchasePowerLessThen =
      averagePurchasePower.length > 1
        ? {
            "additionalData.footfallClassification.averagePurchasePower.end": {
              $lte: Number(averagePurchasePower[1]),
            },
          }
        : {};
    const averageAgeGroupGreaterThen =
      averageAgeGroup.length > 1
        ? {
            "additionalData.footfallClassification.averageAgeGroup.averageStartAge":
              {
                $gte: Number(averageAgeGroup[0]),
              },
          }
        : {};
    const averageAgeGroupLessThen =
      averageAgeGroup.length > 1
        ? {
            "additionalData.footfallClassification.averageAgeGroup.eaverageEndAgend":
              {
                $lte: Number(averageAgeGroup[1]),
              },
          }
        : {};
    // const kidsFriendlyFilter = {
    //   "additionalData.footfallClassification.kidsFriendly": kidsFriendly,
    // };
    const employmentStatusFilter =
      employmentStatus.length > 0
        ? {
            "additionalData.footfallClassification.employmentStatus": {
              $in: [...employmentStatus],
            },
          }
        : {};
    //additionalData.footfallClassification.crowdMobilityType

    const mobilityFilter =
      mobility.length > 0
        ? {
            "additionalData.footfallClassification.crowdMobilityType": {
              $in: [...mobility],
            },
          }
        : {};

    const highlightsFilter =
      screenHighlights.length > 0
        ? {
            screenHighlights: {
              $in: [...screenHighlights],
            },
          }
        : {};
    const screenAddressFilter =
      location.length > 0
        ? {
            screenAddress: {
              $in: location,
            },
          }
        : {};
    const locationFilter =
      location.length > 0
        ? {
            districtCity: {
              $in: location,
            },
          }
        : {};
    const screenNameFilter = {
      name: { $not: { $regex: "sample", $options: "i" } },
    }; // remove screens filter which name have sample

    const screenCategoryFilter = category ? { category: category } : {};

    // console.log({
    //   ...screenNameFilter,
    //   ...averageAgeGroupGreaterThen,
    //   ...averageAgeGroupLessThen,
    //   ...employmentStatusFilter,
    //   ...highlightsFilter,
    //   ...mobilityFilter,
    //   ...averageAgeGroupGreaterThen,
    //   ...averageAgeGroupLessThen,
    //   ...averagePurchasePowerGreaterThen,
    //   ...averagePurchasePowerLessThen,
    //   ...averageDailyFootfallFilter,
    //   ...screenCategoryFilter,
    //   screenAddressFilter,
    //   locationFilter,
    // });
    const screens = await Screen.find({
      // ...averageAgeGroupGreaterThen,
      // ...averageAgeGroupLessThen,
      // ...employmentStatusFilter,
      // ...highlightsFilter,
      // ...mobilityFilter,
      // ...averageAgeGroupGreaterThen,
      // ...averageAgeGroupLessThen,
      // ...averagePurchasePowerGreaterThen,
      // ...averagePurchasePowerLessThen,
      // ...averageDailyFootfallFilter,
      // ...screenCategoryFilter,
      $or: [screenAddressFilter, locationFilter],
    });
    console.log("Result found with screens : ", screens.length);
    return screens;
  } catch (error) {
    throw new Error(error);
  }
};
