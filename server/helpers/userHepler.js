import bcrypt from "bcryptjs";
import User from "../models/userModel.js";
import Campaign from "../models/campaignModel.js";
import Screen from "../models/screenModel.js";
import { returnUserSignIn } from "./userSignin.js";
import { ObjectId } from "mongodb";
import { createNewWalletHelper } from "./userWalletHelper.js";

export const createNewUserHelper = async ({
  name,
  email,
  password,
  avatar,
}) => {
  try {
    const user = new User({
      name: name,
      email: email.toLowerCase(),
      password: bcrypt.hashSync(password, 8),
      avatar:
        avatar ||
        "https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Ftse3.mm.bing.net%2Fth%3Fid%3DOIP.c6ASmT7d2qYobP4OPwAxVgAAAA%26pid%3DApi&f=1",
    });
    const createdUser = await user.save();
    console.log("Successfullt User created : ", createdUser);
    // now create it wallet
    // Note: createNewWalletHelper user with wallet
    const walletUser = await createNewWalletHelper(createdUser._id);
    return returnUserSignIn(walletUser);
  } catch (error) {
    throw new Error(error);
  }
};

export const getUserCampaignsGroupByCidAndCampaignNameHelper = async (
  allyId,
  status
) => {
  try {
    console.log(
      "getUserCampaignsGroupByCidAndCampaignNameHelper called! ",
      status
    );
    // first find all distinct  cid and campaign name
    const data = await Campaign.aggregate([
      { $match: { ally: new ObjectId(allyId) } },
      {
        $group: {
          _id: {
            cid: "$cid",
            campaignName: "$campaignName",
          },
        },
      },
    ]);

    console.log("Data : ", data.length);

    if (data.length === 0) {
      return [];
    }

    const myCampaigns = [];
    const statusFilter = status === "ALL" ? {} : { status };

    for (let singleData of data) {
      let campaign = await Campaign.findOne({
        cid: singleData._id.cid,
        campaignName: singleData._id.campaignName,
        ally: allyId,
        ...statusFilter,
      });

      if (campaign) {
        const campaigns = await Campaign.find({
          cid: singleData._id.cid,
          campaignName: singleData._id.campaignName,
          ally: allyId,
          ...statusFilter,
        });

        const screens = await Screen.find({
          _id: { $in: campaigns?.map((campaign) => campaign.screen) },
        });

        campaign["listofScreens"] = screens;
        campaign["listOfCampaigns"] = campaigns;
        myCampaigns.push({
          campaign,
          listOfScreens: screens,
          listOfCampaigns: campaigns,
        });
      }
    }

    const campaigns = myCampaigns.sort(
      (objA, objB) =>
        new Date(objB.campaign.startDate) - new Date(objA.campaign.startDate)
    );
    console.log("campaigns : ", campaigns.length);

    return campaigns;
  } catch (error) {
    console.log(
      "Error in getUserCampaignsGroupByCidAndCampaignNameHelper: ",
      error
    );
    throw new Error(error);
  }
};
