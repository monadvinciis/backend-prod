import Campaign from "../models/campaignModel.js";
import UserWallet from "../models/userWalletModel.js";
import Screen from "../models/screenModel.js";
import {
  addNewTransaction,
  generateRemarkForCraditAmountToAlly,
  generateRemarkForCraditAmountToScreenOwner,
} from "./userWalletHelper.js";
import {
  CAMPAIGN_STATUS_ACTIVE,
  CAMPAIGN_STATUS_COMPLETED,
  CAMPAIGN_STATUS_DELETED,
  CAMPAIGN_STATUS_PAUSE,
  TRANSACTION_TYPE_CREDIT,
} from "../Constant/campaignStatusConstant.js";

export const createNewCampaignHepler = async ({
  data,
  screen,
  media,
  user,
}) => {
  try {
    const cid = data.cid;
    console.log("createNewCampaignHepler called! : ", data);

    const screenOwnerWallet = await UserWallet.findOne({ user: screen.master });
    if (!screenOwnerWallet) {
      throw new Error("Screen owner has no wallet");
    }

    const newCampaign = new Campaign({
      screen: screen._id,
      screenName: screen.name || "screen name",
      media: media._id,
      cid: cid,
      thumbnail: media.thumbnail,
      campaignName: data.campaignName || "campaign Name",
      video: media.media,
      ally: user._id,
      master: screen.master,
      isSlotBooked: false,
      paidForSlot: false,
      brandName: data.brandName,
      uploadedBy: user.name,
      rentPerDay: screen.rentPerDay,
      slotsPlayPerDay: screen.slotsPlayPerDay,

      totalSlotBooked:
        screen.slotsPlayPerDay * data.noOfDays || screen.slotsPlayPerDay,
      remainingSlots:
        screen.slotsPlayPerDay * data.noOfDays || screen.slotsPlayPerDay,
      slotPlayedPerDay: 0,

      rentPerSlot: (screen.slotsPlayPerDay / screen.rentPerDay).toFixed(2),
      totalAmount: screen.rentPerDay * data.noOfDays || screen.rentPerDay,

      vault: Number(screen.rentPerDay) * Number(data.noOfDays),
      revenue: Number(screen.rentPerDay) * Number(data.noOfDays),

      brandWalletAddress: user.userWallet, // money deduct or refund onthis wallet address
      masterWalletAddress: screenOwnerWallet._id, //on campaign complete add totalAmount in this screen owner wallet
      status: data.status,
      startDate: data.startDate || new Date(),
      endDate: data.endDate || new Date(),
      isDefaultCampaign: data.isDefaultCampaign,
      additionalInfo: data.additionalInfo || {},
    });

    const campaign = await newCampaign.save();
    console.log("campaign created successfully!");
    return campaign;
  } catch (error) {
    throw new Error(error);
  }
};

export const createDefaultCampaign = async ({ data, screen, media, user }) => {
  try {
    const cid = data.cid;
    console.log("createDefaultCampaign called! : ", data);

    const newCampaign = new Campaign({
      screen: screen._id,
      screenName: screen.name || "screen name",
      media: media._id,
      cid: cid,
      thumbnail: media.thumbnail,
      campaignName: data.campaignName || "campaign Name",
      video: media.media,
      ally: user._id,
      master: screen.master,
      isSlotBooked: false,
      paidForSlot: false,
      brandName: data.brandName,
      uploadedBy: user.name,
      status: data.status,
      startDate: data.startDate || new Date(),
      endDate: data.endDate || new Date(),
      isDefaultCampaign: data.isDefaultCampaign,
      additionalInfo: data.additionalInfo || {},
    });

    const campaign = await newCampaign.save();
    console.log("default campaign created successfully!");
    return campaign;
  } catch (error) {
    throw new Error(error);
  }
};

export const changeCampaignStatusAsDeleted = async (campaign) => {
  try {
    const screen = await Screen.findById(campaign.screen);
    // check this campaign uploaded by screen owner or other
    console.log(
      "campaign.ally == campaign.master : ",
      campaign.ally,
      campaign.master,
      campaign.ally.equals(campaign.master)
    );
    if (campaign.ally.equals(campaign.master)) {
      // simply change status as deleetd
      console.log("This campaign uplaoded by screen owner only");
      campaign.status = CAMPAIGN_STATUS_DELETED;
      const updatedCampaign = await campaign.save();
      // const deletedCampaign = await Campaign.deleteOne({ _id: campaign._id });
      const updatedScreen = await Screen.updateOne(
        { _id: campaign.screen },
        { $pull: { campaigns: campaign._id } }
      );
      console.log("campaign status chnage to deleted!");
      return updatedCampaign;
    } else {
      console.log("This campaign uplaoded by Creator only");

      let amountToPayScreenOwner = 0;
      if (campaign.slotPlayedPerDay >= campaign.slotsPlayPerDay) {
        amountToPayScreenOwner = campaign.rentPerDay;
      } else {
        amountToPayScreenOwner =
          campaign.rentPerSlot * campaign.slotPlayedPerDay;
      }
      campaign.vault -= amountToPayScreenOwner;

      if (amountToPayScreenOwner > 0) {
        console.log("addAmountInscreenOwer  when campaign deleted : ");

        const addAmountInscreenOwer = await addNewTransaction({
          toUser: campaign.master, //screen owner
          amount: amountToPayScreenOwner,
          campaign,
          screen,
          remark: generateRemarkForCraditAmountToScreenOwner({
            amount: amountToPayScreenOwner,
            campaignName: campaign.campaignName,
            totalSlotPlayed: campaign.slotPlayedPerDay,
            rentPerSlot: campaign.rentPerSlot,
            screenName: screen.name,
          }),
          type: TRANSACTION_TYPE_CREDIT,
          paymentStatus: true,
        });
      }

      if (campaign.vault > 0) {
        console.log(
          "addAmount to campaign owner  when campaign deleted : ",
          generateRemarkForCraditAmountToAlly({
            amount: campaign.vault,
            campaignName: campaign.campaignName,
            screenName: screen.name,
          })
        );

        const returnAmountToAllyForRemainingSlot = await addNewTransaction({
          toUser: campaign.ally, //
          amount: campaign.vault,
          campaign,
          screen,
          remark: generateRemarkForCraditAmountToAlly({
            amount: campaign.vault,
            campaignName: campaign.campaignName,
            screenName: screen.name,
          }),
          type: TRANSACTION_TYPE_CREDIT,
          paymentStatus: true,
        });
      }
      // now transaction has done for both the user
      //now update campaign status as deleted ans valut = 0
      if (campaign.vault == 0) {
        campaign.status = CAMPAIGN_STATUS_COMPLETED;
      } else {
        campaign.status = CAMPAIGN_STATUS_DELETED;
      }
      campaign.vault = 0;

      const updatedCampaign = await campaign.save();
      // NOW REMOVE THIS CAMPAIGN SCREEN
      const updatedScreen = await Screen.updateOne(
        { _id: campaign.screen },
        { $pull: { campaigns: updatedCampaign._id } }
      );
      console.log("Also camapign removed from screen");
      return updatedCampaign;
    }
  } catch (error) {
    console.log("Error in changeCampaignStatusAsDeleted : ", error);
    throw new Error(error);
  }
};

export const changeCampaignStatusAsPause = async (campaign) => {
  try {
    campaign.status = CAMPAIGN_STATUS_PAUSE;
    const updatedCampaign = await campaign.save();
    console.log("changeCampaignStatusAsPause : ", updatedCampaign.status);
    return updatedCampaign;
  } catch (error) {
    console.log("changeCampaignStatusAsPause : ", error);
    throw new Error(error);
  }
};

export const changeCampaignStatusAsActive = async (campaign) => {
  try {
    campaign.status = CAMPAIGN_STATUS_ACTIVE;
    const updatedCampaign = await campaign.save();
    console.log("changeCampaignStatusAsActive : ", updatedCampaign.status);
    return updatedCampaign;
  } catch (error) {
    console.log("changeCampaignStatusAsActive : ", error);
    throw new Error(error);
  }
};

export const changeCampaignStatusAsCompleted = async (campaign) => {
  try {
    campaign.status = CAMPAIGN_STATUS_COMPLETED;
    const updatedCampaign = await campaign.save();
    console.log("changeCampaignStatusAsCompleted : ", updatedCampaign.status);
    // NOW REMOVE THIS CAMPAIGN SCREEN
    const updatedScreen = await Screen.updateOne(
      { _id: campaign.screen },
      { $pull: { campaigns: updatedCampaign._id } }
    );
    console.log("Also camapign removed from screen");

    return updatedCampaign;
  } catch (error) {
    console.log("changeCampaignStatusAsCompleted : ", error);
    throw new Error(error);
  }
};

export function getAllCampaignStatus(campaigns, userId) {
  let dd = campaigns?.reduce(
    (accum, current) => {
      if (
        current.status === CAMPAIGN_STATUS_ACTIVE ||
        current.status === CAMPAIGN_STATUS_PAUSE
      ) {
        accum.active++;
      } else if (current.status === CAMPAIGN_STATUS_COMPLETED) {
        accum.completed++;
      } else if (current.status === CAMPAIGN_STATUS_DELETED) {
        accum.deleted++;
      } else {
        accum.pending++;
      }
      if (current.ally == userId) {
        accum.myCampaigns++;
      } else {
        accum.othersCampaigns++;
      }

      return accum;
    },
    {
      active: 0,
      pending: 0,
      deleted: 0,
      completed: 0,
      myCampaigns: 0,
      othersCampaigns: 0,
    }
  );

  return dd;
}

export const deleteCampaignsPermanently = async (campaignIds) => {
  try {
    const deletedCampaigns = await Campaign.deleteMany({
      _id: { $in: campaignIds },
    });
    console.log("campaign deleted paranents successfull : ", deletedCampaigns);
    return deletedCampaigns;
  } catch (error) {
    throw new Error(error);
  }
};
