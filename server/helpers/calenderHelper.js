import Calender from "../models/calenderModel.js";

export const createNewCalenderHelper = async ({
  calenderId,
  screenId,
  screenName,
}) => {
  try {
    const calender = new Calender({
      _id: calenderId,
      screen: screenId,
      screenName: screenName,
      slotDetails: [],
      dayDetails: [],
      createdOn: Date.now(),
    });

    const newCalender = await calender.save();
    return newCalender;
  } catch (error) {
    throw new Error(error);
  }
};
