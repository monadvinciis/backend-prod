import express from "express";
import {
  addNewCampaignForMultipleScreen,
  filterScreensBasedOnAudiancesProfile,
} from "../controllers/campaignForMultipleScreenController.js";
const campaignForMultipleScreenRouter = express.Router();

//post

campaignForMultipleScreenRouter.post(
  "/create",
  addNewCampaignForMultipleScreen
);
campaignForMultipleScreenRouter.post(
  "/getScreens",
  filterScreensBasedOnAudiancesProfile
);

//get

//put

//delete

export default campaignForMultipleScreenRouter;
