import express from "express";
import { isAuth } from "../utils/authUtils.js";

import {
  addNewQuery,
  getAllQuery,
  getAllQueryByUser,
} from "../controllers/userQueryController.js";
const userQueryRouter = express.Router();

//post
userQueryRouter.post("/create", isAuth, addNewQuery);

//get
userQueryRouter.get("/user", isAuth, getAllQueryByUser);
userQueryRouter.get("/All", getAllQuery);

export default userQueryRouter;
