import express from "express";
import {
  addNewCampaign,
  getCampaignAndMediaListByScreenId,
  getCampaignDetailByCampaignId,
  getAllActiveCampaignList,
  getCampaignListByScreenName,
  updateCampaignById,
  changeCampaignStatus,
  getAllCampaignListByScreenId,
  getFilteredCampaignListBydateRange,
  getAllCampaignListByBrandId,
  getCampaignDetailWithCidAndCampaignName,
  getCampaignLogsByScreenIdAndCid,
  getAllActiveOrPauseCampaignListByScreenId,
  getAllActiveCampaignListByScreenId,
  getAllActiveOrPauseCampaignListByUserId,
  getAllCampaignListByUserId,
  getAllActiveCampaignListByUserId,
  getTopCampaigns,
  getCamDataByCampaignId,
  getCamDataByCid,
} from "../controllers/campaignController.js";

import { isAuth } from "../utils/authUtils.js";

const campaignRouter = express.Router();

//post request
campaignRouter.post("/create", isAuth, addNewCampaign); // only for screen owner

//get request
campaignRouter.get("/top-campaigns", getTopCampaigns); // not user Ui

campaignRouter.get("/brandCampaign/:brandId", getAllCampaignListByBrandId);
campaignRouter.get("/campaignLogs", getCampaignLogsByScreenIdAndCid);
campaignRouter.get("/camData/campaign", getCamDataByCampaignId);
campaignRouter.get("/camData/logs", getCamDataByCid);

campaignRouter.get("/all", getAllActiveCampaignList);

// Get all campaign list by screen id

campaignRouter.get(
  "/:id/screen/all/activeOrPauser",
  getAllActiveOrPauseCampaignListByScreenId
);
campaignRouter.get("/:id/screen/all", getAllCampaignListByScreenId);
campaignRouter.get(
  "/:id/screen/all/active",
  getAllActiveCampaignListByScreenId
); // not implemented in ui

// Get all campaign list by user id

campaignRouter.get(
  "/:id/user/all/activeOrPauser",
  getAllActiveOrPauseCampaignListByUserId
);
campaignRouter.get("/:id/user/all", getAllCampaignListByUserId);
campaignRouter.get("/:id/user/all/active", getAllActiveCampaignListByUserId); // not implemented in ui

campaignRouter.get("/:name/screenName", getCampaignListByScreenName);

// get campaign details
campaignRouter.get("/:id", getCampaignDetailByCampaignId);
campaignRouter.get(
  "/:cid/:campaignName",
  getCampaignDetailWithCidAndCampaignName
);
campaignRouter.get(
  "/:id/screen/campaignAndMedia",
  getCampaignAndMediaListByScreenId
);
campaignRouter.get(
  "/filterCampaignByDate/:startValue/:endValue/:userId/:screenId",
  getFilteredCampaignListBydateRange
);

//put request
campaignRouter.put("/:id", updateCampaignById); // not used in UI
campaignRouter.put("/changeStatus/:id/campaign/:status", changeCampaignStatus);
//delete request

export default campaignRouter;
