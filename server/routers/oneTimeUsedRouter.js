import express from "express";
import {
  addFieldInScreenCollection,
  addScreenNameInAllCampaigns,
  deleteCampaignsPermanentlyByUserId,
} from "../utils/oneTimeFunctionUsed.js";

const oneTimeUseRouter = express.Router();

// put

oneTimeUseRouter.put(
  "/updateCampaignWithScreenName/all",
  addScreenNameInAllCampaigns
);

oneTimeUseRouter.put("/addFiedInScreen", addFieldInScreenCollection);

// delete
oneTimeUseRouter.delete(
  "/deleteCamapigns/:id",
  deleteCampaignsPermanentlyByUserId
); // not used in UI

export default oneTimeUseRouter;
