import express from "express";
import {
  createNewScreen,
  addReview,
  deleteScreenById,
  getPinDetailsByScreenId,
  getScreenDetailsByScreenId,
  getScreenPlayList,
  getTopSixScreenList,
  updateScreenById,
  syncScreenCodeForApk,
  getScreenDetailsForApk,
  checkScreenPlaylistForApk,
  getScreenLogsByScreenId,
  addAllyPleaRequestForScreen,
  giveAccessToAllyPlea,
  rejectAllayPlea,
  getFilteredScreenList,
  getFilteredScreenListByAudiance,
  getAllScreens,
  getCouponListByScreenId,
  getScreensBySearchQuery,
  camDataHandleScreen,
  // genderAgeCamDataHandleScreen,
  // impressionCamDataHandleScreen,
  getScreensByUserIds,
  getScreensByScreenIds,
  getScreenCamData,
  getScreensByCampaignIds,
  // saveAlphaCamera,
  getAllScreenList,
  enterScreenPlaybackLogs,
  getScreenCampaignsByUserId,
} from "../controllers/screenController.js";
import { isAuth } from "../utils/authUtils.js";
import {
  getQrScanData,
  getScreenDataByScreenId,
  getScreenDataByDate,
  scanQrDataSave,
} from "../controllers/screenDataController.js";

const screenRouter = express.Router();

//post request
screenRouter.post("/", isAuth, createNewScreen);
screenRouter.post("/:id/reviews", isAuth, addReview);
screenRouter.post("/:id/allyPlea/ally", isAuth, addAllyPleaRequestForScreen);

//get request
screenRouter.get("/", getTopSixScreenList);
screenRouter.get("/all", getAllScreenList);
screenRouter.get("/:id", getScreenDetailsByScreenId);
screenRouter.get("/:id/user", getScreenCampaignsByUserId);

screenRouter.get("/getScreensByUserIds", getScreensByUserIds);
screenRouter.get("/getScreensByScreenIds", getScreensByScreenIds);
screenRouter.get("/getScreensByCampaignIds", getScreensByCampaignIds);
screenRouter.get("/couponList/:screenId", getCouponListByScreenId);
screenRouter.get("/:screenId/screenLogs", getScreenLogsByScreenId);

// Get filter screens
screenRouter.get("/getScreens", getScreensBySearchQuery);
screenRouter.get("/getFilterScreenList/:text/:locality", getFilteredScreenList);
screenRouter.get(
  "/getFilteredScreenListByAudiance/:averageDailyFootfall/:averagePurchasePower/:averageAgeGroup/:employmentStatus/:kidsFriendly",
  getFilteredScreenListByAudiance
);

screenRouter.get("/:id/pin", getPinDetailsByScreenId);
screenRouter.get("/:id/screenmedias/playlist", getScreenPlayList); // not understand what is its purpose ?

//put request
screenRouter.put("/:id", isAuth, updateScreenById);
screenRouter.put("/:id/allyPlea/master", giveAccessToAllyPlea);
screenRouter.put("/:id/allyPlea/reject", rejectAllayPlea);

//delete request

screenRouter.delete("/:id", isAuth, deleteScreenById);

// android apk related
screenRouter.get("/syncCode/:syncCode", syncScreenCodeForApk);
screenRouter.get("/:name/screenName", getScreenDetailsForApk);
screenRouter.get(
  "/:name/screenName/:time/:currentVideo",
  checkScreenPlaylistForApk
);

// screendata related
screenRouter.get("/screenData/:screenId", getScreenDataByScreenId);
screenRouter.get(
  "/todayScreenData/:id/pageNumber/:pageNumber",
  getScreenDataByDate
);

// screen qr related
screenRouter.put("/scanqrdata/:screenId", scanQrDataSave);
screenRouter.get("/qrscandata/:screenId", getQrScanData);

screenRouter.get("/get/allScreens", getAllScreens);

// pi and cam related
screenRouter.put("/cam/:screenId", camDataHandleScreen);
// screenRouter.put("/genderagecam/:screenId", genderAgeCamDataHandleScreen);
// screenRouter.put(
//   "/impressionMultiplier/cam/:screenId",
//   impressionCamDataHandleScreen
// );
// screenRouter.put("/alpha/:screenId", saveAlphaCamera);
screenRouter.put("/enterplaybacklogs/:screenId", enterScreenPlaybackLogs);
screenRouter.get("/getscreencamdata/:screenId", getScreenCamData);

export default screenRouter;
