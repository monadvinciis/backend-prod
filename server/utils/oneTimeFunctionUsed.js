import Campaign from "../models/campaignModel.js";
import Screen from "../models/screenModel.js";

export const addScreenNameInAllCampaigns = async (req, res) => {
  try {
    console.log("addScreenNameInAllCampaigns called!");
    const campaigns = await Campaign.find({});
    console.log("Total campaigns found! : ", campaigns.length);

    for (let campaign of campaigns) {
      const screen = await Screen.findById(campaign.screen);
      if (screen) campaign.screenName = screen.name || "screen name";
      else campaign.screenName = "screen name";
      let updatedCampaign = await campaign.save();
      console.log("Campaign addedd screen : ", updatedCampaign.screenName);
    }
    return res.status(200).send("Every thing going correct");
  } catch (error) {
    console.log("Error in addScreenNameInAllCampaigns : ", error);
    return res
      .status(500)
      .send({ message: `Error in addScreenNameInAllCampaigns :  ${error} ` });
  }
};

export async function deleteCampaignsPermanentlyByUserId(req, res) {
  try {
    const userId = req.params.id || "63f9ca13f179e67ed6c3357b";

    // first check user present or not
    const user = await User.findById(userId);
    if (!user) {
      return res.status(404).send({ message: "user not found" });
    }
    // let campaigns = await Campaign.find({ ally: userId });
    // console.log(" before delete camapigns : ", campaigns.length);

    // // direct delete
    // const deletedCampaigns = await Campaign.deleteMany({ ally: userId });
    // console.log("all deleted campaigns : ", deletedCampaigns);

    // campaigns = await Campaign.find({ ally: userId });
    // console.log(" After deletecamapigns : ", campaigns.length);

    //screen wise delete
    // first find all screens of user
    const screens = await Screen.find({ master: userId });
    //iterate each screens and delete each campaigns
    for (let screen of screens) {
      let campaigns = screen.campaigns;
      // console.log(`campaigns of ${screen.name} : ${campaigns}`);
      for (let campaignId of campaigns) {
        const deletdCampaigns = await Campaign.deleteOne({ _id: campaignId });
        // console.log("deleted campaigns : ", deletdCampaigns);
      }
      screen.campaigns = [];
      await screen.save();
      // delete screen also if need else collent that line
    }
    return res
      .status(200)
      .send({ message: "All campaigns deleted successfully!" });
  } catch (error) {
    return res
      .status(404)
      .send({ message: `campaign router error, ${error.message}` });
  }
}

export const addFieldInScreenCollection = async (req, res) => {
  try {
    const updatedScreens = await Screen.updateMany(
      {},
      {
        $set: {
          rentPerDay: 100,
          slotsPlayPerDay: 100,
        },
      }
    );

    console.log("updatedScreens : ", updatedScreens);
    return res.status(200).send("Added filed inscreen collection success");
  } catch (error) {
    return res.status(500).send("Errro in : ", error);
  }
};
